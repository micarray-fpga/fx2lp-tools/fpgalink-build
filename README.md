# fpgalink-build

This repository is a wrapper to build the fpgalink tools made by
[Chris McClelland](https://github.com/makestuff).
The official documentation on how to build the tools is outdated
and contains dead links, which makes it challenging.

With some trial and error, I found a way to build the tools using `cmake`,
thus this repository was created.
The required libraries, forked from github, are included as submodules.

## Dependencies

On ubuntu based distros, install the dependencies with this command:

```
sudo apt install build-essential cmake libusb-1.0-0-dev
```

## Building

1. Clone this wrapper including the submodules
	(it is assumed you have `git` installed and preferably configured)

	```
	git clone --recurse-submodules https://gitlab.com/micarray-fpga/fx2lp-tools/fpgalink-build.git
	```

2. Create and enter the `build` subdirectory

	```
	cd fpgalink-build
	mkdir build || cd build
	```

3. Build with `cmake`

	```
	cmake ..
	make
	```

4. Install the tools (with superuser privileges)

	```
	sudo make install
	sudo ldconfig
	```

5. Done

